from socket import *
from collections import defaultdict
import sys
#broker
MAX_BUF = 2048

addr_serv = ('localhost', 50000)
s = socket(AF_INET, SOCK_DGRAM)
s.bind(addr_serv)

connect_ = defaultdict(list)

addr_sertosub = 0
topic_pub = ''
topic_sub = ''
pub_payload = ''
typ = ''
cmd = ''
i = 0

while(1):
	cmd_payload, addr = s.recvfrom(MAX_BUF)
	cmd_payload = cmd_payload.decode('utf-8')


	if ' ' in cmd_payload:
		cmd, payload, typ = cmd_payload.split(' ')

		if cmd == 'cancel' and typ == 'pub' and topic_pub !='':
			txtout = 'cancelled'
			for i in range(len(connect_[topic_pub])):
				s.sendto(txtout.encode('utf-8'), connect_[topic_pub][i])
			topic_pub = ''
			connect_.clear()
			print('Topic is cancelled')
			continue

		elif cmd == 'topic' and typ == 'pub' and topic_pub == '':
			topic_pub = payload
			print("Topic for publisher :",topic_pub)
			if topic_pub == topic_sub:
				pub_payload = 'topic is match'
				for i in range(len(connect_[topic_pub])):
					s.sendto(pub_payload.encode('utf-8'), connect_[topic_pub][i])

		elif cmd == 'topic' and typ == 'pub' and topic_pub != '':
			print('Please cancel the topic before publish a new topic')

		elif cmd == 'publish' and topic_pub != '':
			pub_payload = payload
			print('Topic is '+topic_pub+', Payload is '+pub_payload)

			for i in range(len(connect_[topic_pub])):
				s.sendto(pub_payload.encode('utf-8'), connect_[topic_pub][i])

		elif cmd == 'publish' and topic_pub == '':
			pub_payload = ''
			print('Please public your topic')

		elif cmd == 'topic' and typ == 'sub':
			topic_sub = payload
			addr_sub = addr

			connect_[topic_sub].append(addr_sub)
			print(connect_.keys())
			print(connect_[topic_sub][0])

			print(connect_)
			if topic_pub == topic_sub:
				pub_payload = 'topic is match'
				s.sendto(pub_payload.encode('utf-8'), addr_sertosub)
	else:
		print('Please enter your command')
		continue
s.close()


