from socket import * 
import sys
#subscriber
MAX_BUF = 2048
addr_serv = ('localhost',50000)
s = socket(AF_INET, SOCK_DGRAM)

def input_cmd_topic():
	cmd_topic = input('Enter your cmd_topic to subsciber : ')
	cmd_topic = cmd_topic + ' sub'
	return cmd_topic.encode('utf-8')

while(1):
	cmd_topic = input_cmd_topic()
	s.sendto(cmd_topic, addr_serv)
	while(1):
		payload, addr = s.recvfrom(MAX_BUF)
		print(payload.decode('utf-8'))
		if payload.decode('utf-8') == 'cancelled':
			break
s.close()