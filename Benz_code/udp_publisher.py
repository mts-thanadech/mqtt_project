#!/usr/bin/env python
from socket import *
import sys

MAX_BUF = 2048
SERV_PORT = 50001
addr = ("127.0.0.1", SERV_PORT)
s = socket(AF_INET, SOCK_DGRAM)

textOut = "1,null,connect,null"
s.sendto(textOut.encode("utf-8"), addr)

state = 0

while True:

	sys.stdin.flush()
	msg = input("command> ")

	if msg.find(" ") < 0:
		print("Command ERROR, Command Supports : topic, publish, cancel, close")
		continue

	command, data = msg.split(" ")
	command = command.lower()

	if command != "topic" and command != "publish" and command != "cancel" and command != "close":
		print("Command ERROR, Command Supports : topic, publish, cancel, close")
		continue

	if command == "topic" and state == 0:
		state = 1
		topic = data
		textOut = "1,null,topic,"+data
		s.sendto(textOut.encode("utf-8"), addr)

	elif command == "publish" and state == 1:
		textOut = "1,"+topic+",publish,"+data
		s.sendto(textOut.encode("utf-8"), addr)

	elif command == "cancel" and state == 1 and topic == data:
		state = 0
		textOut = "1,null,cancel,"+data
		s.sendto(textOut.encode("utf-8"), addr)

	elif command == "close":
		textOut = "1,null,close,null"
		s.sendto(textOut.encode("utf-8"), addr)
		break
		
	else:
		if state == 1 and topic != data:
			print("Command ERROR, Don't have topic %s" %(data))
		elif state == 1:
			print("Command ERROR, Can publishs one topic at a time")
		elif state == 0:
			print("Command ERROR, Have to enter topic first")
s.close()