#!/usr/bin/env python
from socket import *
import sys

MAX_BUF = 1024
SERV_PORT = 50001
addr = ("127.0.0.1", SERV_PORT)
s = socket(AF_INET, SOCK_DGRAM)

textOut = "2,null,connect,1"
s.sendto(textOut.encode("utf-8"), addr)

state = 0

while True:

	sys.stdin.flush()
	msg = input("command> ")

	if msg.find(" ") < 0:
		print("Command ERROR, Command Supports : topic")
		continue

	command, data = msg.split(" ")
	command = command.lower()

	if command != "topic":
		print("Command ERROR, Command Supports : topic")
		continue
	else:
		textOut = "2,null,topic,"+data
		s.sendto(textOut.encode("utf-8"), addr)
		
		while True:
			msgIn, msgAddr = s.recvfrom(MAX_BUF)
			if msgIn.decode("utf-8") == "close":
				print("Special command : close")
				state = 1
				break
			else:
				print("Message from topic %s> %s" %(data, msgIn.decode("utf-8")))

		if state == 1:
			break
s.close()