from socket import *
import sys

SERV_PORT = 50001
addr = ("127.0.0.1", SERV_PORT)
s = socket(AF_INET, SOCK_DGRAM)
s.bind(addr)
print("Broker started ...", end="\n\n")

subscriberTopic = []
matchedPort = []

while True:

	dataIn, dataAddr = s.recvfrom(1024)
	dataIn = dataIn.decode("utf-8")

	typ, topic, command, data = dataIn.split(',')

	if command == "connect":
		print("New client connected, port : ", dataAddr[1], end="\n\n")
	else:
		if typ == '1':
			print("Publisher from %s port %s" %(dataAddr[0], dataAddr[1]))
			print("Command : %s" %(command))
			print("Data : %s" %(data), end="\n\n")

			if command == "close":
				print("Special command : close", end="\n\n")
				for j in range(len(subscriberTopic)):
					subPort, subTopic = subscriberTopic[j].split(',')
					subAddr = ("127.0.0.1", int(subPort))
					dataOut = "close"
					s.sendto(dataOut.encode("utf-8"), subAddr)
				break
			elif command == "publish":
				state = 0

				for i in range(len(subscriberTopic)):
					subPort, subTopic = subscriberTopic[i].split(',')
					if topic == subTopic:
						for j in range(len(matchedPort)):
							if subPort == matchedPort[j]:
								state = 1
								break
						if state != 1:
							matchedPort.append(subPort)

				for i in range(len(matchedPort)):
					subAddr = ("127.0.0.1", int(matchedPort[i]))
					s.sendto(data.encode(), subAddr)
				for i in range(len(matchedPort)):
					matchedPort.pop()

		elif typ == '2':
			print ("Subscriber from %s port %s" %(dataAddr[0], dataAddr[1]))
			print ("Follow topic : %s" %(data), end="\n\n")
			topic = str(dataAddr[1])+","+data
			subscriberTopic.append(topic)
			
s.close()